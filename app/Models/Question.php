<?php

namespace App\Models;

use App\Helpers\Votable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Question extends Model
{
    use HasFactory;
    use Votable;
    protected $guarded = ['id'];

    /**
     * Accessors
     */

    public function getFavouritesCountAttribute()
    {
        return $this->favourites->count();
    }

    public function getUrlAttribute(){
        return "questions/{$this->slug}";
    }

    public function getCreatedDateAttribute(){
        return $this->created_at->diffForHumans();
    }

    public function getAnswerStyleAttribute(){
        if($this->answers_count > 0){
            if($this->best_answer_id){
                return "has-best-answer";
            }
            return "answered";
        }
        return "unanswered";
    }

    public function getFavouriteStyleAttribute(){
        if($this->getIsFavouriteAttribute()){
            return 'text-success';
        }
        return "text-black-50";
    }

    public function getIsFavouriteAttribute(){
        return $this->favourites()->where('user_id', auth()->id())->count() > 0;
    }

    /**
     * MUTATORS
     */

    public function setTitleAttribute(string $title){
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }

    public function markBestAnswer(Answer $answer){
        $this->best_answer_id = $answer->id;
        $this->save();
    }
    /**
     * Relationship methods
     *
     */
    public function owner(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function answers(){
        return $this->hasMany(Answer::class);
    }

    public function favourites(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function votes(){
        return $this->morphToMany(User::class,'vote')->withTimestamps();
    }
}
